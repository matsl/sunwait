CFLAGS = -g -Wall -Wextra -pedantic
LIBS = -lm
OBJS = main.o print.o sunriset.o

sunwait: $(OBJS)
	$(CC) -o sunwait $(CFLAGS) $(OBJS) -lm

clean:
	rm -f *.o *~ .*~ sunwait
