# README #

## Description 

Sunwait calculates sunrise or sunset times with civil, nautical,
astronomical and custom twilights. 

This project is a fork from the work by Dan Risacher. See
http://risacher.org/sunwait/

There also exists another fork
https://sourceforge.net/projects/sunwait4windows/ which Dan Risacher
considers to be the current version. 

For my purposes the original Dan Risachers version was a more natural
choise which boils down to these additions. New option -t and -o.

    -t Don't sleep, just exit
    -o Exit with status code 1 if next event is past midnight 
   
## Build

Type 'make'

## Usage

    usage: sunwait [options] [sun|civ|naut|astr] [up|down] [+/-offset] [latitude] [longitude]

    latitude/longitude are expressed in floating-point degrees, with [NESW] appended

    example: sunwait sun up -0:15:10 38.794433N 77.069450W
    This example will wait until 15 minutes and 10 seconds before the sun rises in Alexandria, VA

    The offset is expressed as MM, or HH:MM, or HH:MM:SS,
    and indicates the additional amount of time to wait after 
    (or before, if negative) the specified event.

    options: -p prints a summary of relevant times
         -z changes the printout to Universal Coordinated Time (UTC)
         -V prints the version number
         -o exit with status code 1 if event already occured today
         -t don't sleep, ie dry run
         -v increases verbosity

    These options are useful mainly when used with the '-p' option
         -y YYYY sets the year to calculate for
         -m MM sets the month to calculate for
         -d DD sets the day-of-month to calculate for

         -h prints this help

## Bugs

Doing home automation close to the poles might need some more thoughts
than described in the Tips section below.

## Tips

For home automation purposes you are likely to activate your morning
scripts from a fixed time by cron. You then need to check if the event
you want to wait for will occur first tomorrow. In that case you
probably don't want to do anythings today. You can check this like
this:

	if ! sunwait -t -o sun up <other args to sunwait>
	then
      echo This will happen first tomorrow ...
      exit
	fi
	...
	
For the similar thing in the evening you most likely don't use cron
for getting the start time right. Instead you let cron start a job
long before the sun sets like at 12 oclock when you are sure the sun
is up. sunwait then does its job right and waits for the sun to
set. In that case you might want to check if it is passed the time of
the evening when your automation should be activated.

This bash shell snippet can come in handy in that case:
	
	bedTime="22:30"
    timeToTurnOff=$(( $(date -d "$bedTime" +%s) - $(date +%s) ))
    if [ "$timeToTurnOff" -le "0" ]
    then
      echo We have passed bed time so do nothing
      exit
    fi
	...
