/* Function prototypes */

int print_everything(int year, int month, int day, 
		     double lat, double lon,
		     struct tm* tm, char local);
